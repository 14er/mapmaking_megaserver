package com.fourteener.mapmakingserver.launcher;

import com.fourteener.mapmakingserver.launcher.Misc;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Setup {
	// Some first-time setup stuff, is faster than general updating
    public static void runSetup() throws Exception {
        ArrayList<String> updateData = Setup.parseUpdate(Setup.getUpdates().get(0));
        Setup.installUpdate(updateData.get(0), "server_data");
        Misc.writeToFile("server_data/exec_ver.mms", false, updateData.get(1));
    }

    // Updates the server if it needs to
    public static void updateExec() throws Exception {
        ArrayList<String> updateData;
        // Does it's own thing if beta mode is enabled
        if (new File("server_data/betaMode").exists()) {
            Setup.getBetaExec();
            return;
        }
		    String verStr;
		    if (new File("server_data/exec.version.mms").exists()) {
			      verStr = Misc.readFromFile("server_data/exec_ver.mms");
	    	} else verStr = "0";
        double verNum = Double.parseDouble(verStr.split("\\n")[0]);
        if (verNum == Double.parseDouble((updateData = Setup.parseUpdate(Setup.getUpdates().get(0))).get(1))) {
            return;
        }
        Setup.installUpdate(updateData.get(0), "server_data");
        Misc.writeToFile("server_data/exec_ver.mms", false, updateData.get(1));
    }

    // Does the same thing as updateExec() but will always download the beta build
    public static void getBetaExec() throws Exception {
        String s = Misc.getTextFromURL("http://dl.14ercooper.com/mmsdownloadables/dl_beta.txt");
        Setup.installUpdate(s, "server_data");
    }

    // Gets an array with available updates
    private static ArrayList<String> getUpdates() throws Exception {
        String s = Misc.getTextFromURL(Misc.downloadURL);
        return new ArrayList<String>(Arrays.asList(s.split(";")));
    }

    // Splits stuff up as needed for parsing update data
    private static ArrayList<String> parseUpdate(String s) {
        return new ArrayList<String>(Arrays.asList(s.split("\\s*,\\s*")));
    }

    // Downloads and installs a new server exec
    private static void installUpdate(String updateURL, String filePathTarget) throws IOException, InterruptedException {
        if (!new File(filePathTarget).exists()) {
            new File(filePathTarget).mkdirs();
        }
        new File("temp").mkdirs();
        Setup.downloadFromURL(updateURL, "dlFile.zip");
        Setup.unzipFile("dlFile.zip", "temp");
        Setup.copyDirectory("temp", filePathTarget);
        Setup.delDirectory("temp");
        new File("dlFile.zip").delete();
    }

    // Used to grab a file from a URL
    public static void downloadFromURL(String sourceURL, String target) throws IOException {
        URL website = new URL(sourceURL);
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        FileOutputStream fos = new FileOutputStream(target);
        fos.getChannel().transferFrom(rbc, 0L, Long.MAX_VALUE);
        fos.close();
    }

    // Unzips a folder to a folder
    private static void unzipFile(String fileZip, String targetPath) throws IOException {
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            int len;
            String fileName = zipEntry.getName();
            File newFile = new File(targetPath + "/" + fileName);
            FileOutputStream fos = new FileOutputStream(newFile);
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }

    // Useful for copying folders around
    private static void copyDirectory(String src, String dest) throws IOException {
        File source = new File(src);
        File destination = new File(dest);
        if (source.isDirectory()) {
            @SuppressWarnings("unused")
			String[] files;
            if (!destination.exists()) {
                destination.mkdirs();
            }
            for (String file : files = source.list()) {
                File srcFile = new File(source, file);
                File destFile = new File(destination, file);
                Setup.copyDirectory(srcFile.toString(), destFile.toString());
            }
        } else {
            InputStream in = null;
            OutputStream out = null;
            try {
                int length;
                in = new FileInputStream(source);
                out = new FileOutputStream(destination);
                byte[] buffer = new byte[1024];
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
            }
            catch (Exception buffer) {
                // empty catch block
            }
            in.close();
            out.close();
        }
    }

    // Recursive directory deletion
    private static void delDirectory(String target) throws IOException {
        Path directory = Paths.get(target, new String[0]);
        Files.walkFileTree(directory, (FileVisitor<? super Path>)new SimpleFileVisitor<Path>(){

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

}