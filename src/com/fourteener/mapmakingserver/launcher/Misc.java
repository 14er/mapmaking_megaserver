package com.fourteener.mapmakingserver.launcher;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Misc {
	// Some universal variables
    public static String downloadURL = "https://14erc.com/mmsdownloadables/dl_data.txt";
    public static double JAVA_VERSION = Misc.getVersion();

    // Is the internet connected?
    public static boolean internetConnected() {
        try {
            URL url = new URL(downloadURL);
            URLConnection connection = url.openConnection();
            connection.connect();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    // What version of java is running
    static double getVersion() {
        String version = System.getProperty("java.version");
        if (version.contains("10")) {
        	return 10;
        }
        if (version.contains("11")) {
        	return 11;
        }
        if (version.contains("12")) {
        	return 12;
        }
        if (version.contains("13")) {
        	return 13;
        }
        if (version.contains("14")) {
        	return 14;
        }
        else return 1;
    }

    // Used to read update data from the text file
    public static String getTextFromURL(String url) throws Exception {
        String inputLine;
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    // Saves data to a file on disk
    public static void writeToFile(String file, boolean append, String text) throws IOException {
        FileWriter fileWriter = new FileWriter(file, append);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(text);
        bufferedWriter.close();
        fileWriter.close();
    }

    // Loads data from a file on disk
    public static String readFromFile(String file) throws IOException {
        List<String> fileContentsList = Files.readAllLines(Paths.get(file, new String[0]));
        String fileContents = "";
        for (String i : fileContentsList) {
            fileContents = fileContents + i + "\n";
        }
        return fileContents;
    }

    // Cross platform console clear
    public static void clearConsole() {
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                Runtime.getRuntime().exec("clear");
            }
        }
        catch (IOException | InterruptedException exception) {
            // empty catch block
        }
    }
}