package com.fourteener.mapmakingserver.launcher;

import com.fourteener.mapmakingserver.launcher.Misc;
import com.fourteener.mapmakingserver.launcher.Setup;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws Exception {
    	// Is the server running in dev mode? Is it in a console window?
        boolean devMode = false;
        if (System.console() == null) {
            System.exit(0);
        }

        if (Misc.JAVA_VERSION < 10) {
            System.out.println("Server Error: Requires Java 10 or newer.\nPlease update Java from <java.com/download> before using this server.\nIf you have Java 8 installed, please verify that you have uninstalled old versions of Java.");
            Thread.sleep(5000L);
            System.exit(0);
        }
      
        // Updates the server if needed and not in dev mode
        if (!devMode) {
            boolean connected = Misc.internetConnected();
            boolean serverInitialized = false;
            File rootFolder = new File("server_data");
            if (rootFolder.exists()) {
                serverInitialized = true;
            }
            // Ensures it is connected to the internet the first time to download data
            if (!serverInitialized && !connected) {
                System.out.println("Server Error: Cannot initialize server.\nPlease connect to the internet to download first-time server data.\n\n\nIf this is not the first time you are using the server, your data may have been corrupted.\nPlease verify the presence of the <server_data> folder before continuing.");
                Thread.sleep(5000L);
                System.exit(0);
            }
            // Sets up the server and/or updates it
            if (!serverInitialized) {
                Setup.runSetup();
            } else if (connected) {
                Setup.updateExec();
            }
        }
        // Runs the actual server executable
        try {
            Misc.clearConsole();
            String arguments = "";
            for (String s : args) {
                arguments = arguments + " " + s;
            }
            Process p = Main.runProcess("java -jar server_exec.jar updated3 betaMode" + arguments, "server_data");
            p.waitFor();
        }
        // Makes sure that any errors are caught, though mostly a bad java install
        catch (IOException e) {
            e.printStackTrace();
            System.out.println("Location: com.fourteener.mapmakingserver.launcher.main:51\n\n\nA fatal error occured.\nPlease verify that java is in your system path (type 'java' into a command prompt).\n\nIf java is in your system path, please screenshot this and send it to 14ercooper.");
            Thread.sleep(60000L);
        }
    }

    // Used for running a process
    private static Process runProcess(String cmd, String dir) throws IOException {
        String[] cmdList = cmd.split(" ");
        ProcessBuilder pb = new ProcessBuilder(cmdList);
        pb.directory(new File(dir));
        return pb.inheritIO().start();
    }
}