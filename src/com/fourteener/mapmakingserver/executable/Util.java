package com.fourteener.mapmakingserver.executable;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class Util {
	// Name of current profile (for world updating purposes)
	public static String worldFolderName = "undefined";
	
	// Checks if the user has an active internet connection (using the downloadables file)
	public static String downloadURL= "https://14erc.com/mmsdownloadables/dl_data.txt";
	public static boolean forceOffline = false;
	public static boolean internetConnected () {
		if (forceOffline)
			return false;
		try {
			URL url = new URL(downloadURL);
			URLConnection connection = url.openConnection();
			connection.connect();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	// Gets the current java version and stores it as a double
	public static double JAVA_VERSION = getVersion ();
    static double getVersion() {
    	String version = System.getProperty("java.version");
        if (version.contains("10")) {
        	return 10;
        }
        if (version.contains("11")) {
        	return 11;
        }
        if (version.contains("12")) {
        	return 12;
        }
        if (version.contains("13")) {
        	return 13;
        }
        if (version.contains("14")) {
        	return 14;
        }
        else return 1;
    }
	
	// Returns the contents of a URL as a string
	public static String getTextFromURL (String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                    connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null) 
            response.append(inputLine);

        in.close();

        return response.toString();
    }
	
	// Writes a string to a file
	public static void writeToFile (String file, boolean append, String text) throws IOException {
		FileWriter fileWriter = new FileWriter (file, append);
		BufferedWriter bufferedWriter = new BufferedWriter (fileWriter);
		bufferedWriter.write(text);
		bufferedWriter.close();
		fileWriter.close();
	}
	
	// Writes the data in textList to file, splitting each element with sepChar
	public static void writeToFile (String file, boolean append, ArrayList<String> textList, String sepChar) throws IOException {
		String text = "";
		for (String s : textList) {
			text += s + sepChar;
		}
		FileWriter fileWriter = new FileWriter (file, append);
		BufferedWriter bufferedWriter = new BufferedWriter (fileWriter);
		bufferedWriter.write(text);
		bufferedWriter.close();
		fileWriter.close();
	}
	
	// Reads the data from a file to a string
	public static String readFromFile (String file) throws IOException {
		List<String> fileContentsList = Files.readAllLines(Paths.get(file));
		String fileContents = "";
		for (String i : fileContentsList) {
			fileContents += i + "\n";
		}
		return fileContents;
	}
	
	// Wipes the console clean
	public static void clearConsole() {
	    try {
	        if (System.getProperty("os.name").contains("Windows"))
	            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
	        else
	            Runtime.getRuntime().exec("clear");
	    } catch (Exception ex) {}
	}
	
	// Downloads a .zip, extracts to a temporary folder, copies it into the given directory (making it in the process), then cleans up temp files/folders
	public static void downloadUnzipCopy (String updateURL, String filePathTarget) throws IOException, InterruptedException {
		if (!(new File (filePathTarget).exists()))
			(new File (filePathTarget)).mkdirs(); // Makes the directory(s) for the installation, if it doesn't already exist
		(new File ("temp")).mkdirs(); // Generates a temporary folder to hold stuff after the unzip
		downloadFromURL (updateURL, "dlFile.zip"); // Downloads the update from the server
		unzipFile ("dlFile.zip", "temp"); // Unzips the update to a temporary folder
		copyDirectory ("temp", filePathTarget); // Copies the update to where it belongs
		delDirectory ("temp"); // Cleans up the temporary folder
		(new File ("dlFile.zip")).delete(); // Cleans up the downloaded .zip file
	}
	
	// Downloads the file at the URL to the target
	public static void downloadFromURL (String sourceURL, String target) throws IOException {
		URL website = new URL(sourceURL);
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
		FileOutputStream fos = new FileOutputStream(target);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		fos.close();
	}
	
	// Used to unzip a .zip to the given directory
	public static void unzipFile (String fileZip, String targetPath) throws IOException {
		try {
            // Open the zip file
            ZipFile zipFile = new ZipFile(fileZip);
            Enumeration<?> enu = zipFile.entries();
            while (enu.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) enu.nextElement();
                String name = zipEntry.getName();
                // Do we need to create a directory ?
                File file = new File(targetPath + "/" + name);
                if (name.endsWith("/")) {
                    file.mkdirs();
                    continue;
                }
                File parent = file.getParentFile();
                if (parent != null) {
                    parent.mkdirs();
                }
                // Extract the file
                InputStream is = zipFile.getInputStream(zipEntry);
                FileOutputStream fos = new FileOutputStream(file);
                byte[] bytes = new byte[1024];
                int length;
                while ((length = is.read(bytes)) >= 0) {
                    fos.write(bytes, 0, length);
                }
                is.close();
                fos.close();
            }
            zipFile.close();
        } catch (IOException e) {}
    }
	
	// Moves a file
	public static void moveFile (String src, String dest) throws IOException {
		Files.move(Paths.get(src), Paths.get(dest), StandardCopyOption.REPLACE_EXISTING);
	}
	
	// Used to zip a directory
	public static String zipFolder (String src, String dest) throws InterruptedException {
		return zipDirectory(new File(src), new File(dest));
	}
	public static String zipFolder (String src, String dest, int dim) throws InterruptedException {
		return zipDirectory(new File(src), new File(dest), dim);
	}
	
	public static String zipDirectory (File directoryToCompress, File outputDirectory) throws InterruptedException{
    	String epochTime = Long.toString(Instant.now().getEpochSecond());
        try {
            FileOutputStream dest = new FileOutputStream(new File(outputDirectory, epochTime + ".zip"));
            ZipOutputStream zipOutputStream = new ZipOutputStream(dest);

            zipDirectoryHelper(directoryToCompress, directoryToCompress, zipOutputStream);
            zipOutputStream.close();
        } catch (Exception e) {
        	System.out.println("Zip error");
        	Thread.sleep(2000);
        }
        return epochTime;
    }
	public static String zipDirectory (File directoryToCompress, File outputDirectory, int dim) throws InterruptedException{
    	String epochTime = Long.toString(Instant.now().getEpochSecond());
        try {
        	FileOutputStream dest;
        	if (dim == 0)
        		dest = new FileOutputStream(new File(outputDirectory, epochTime + ".zip"));
        	else if (dim == 1)
        		dest = new FileOutputStream(new File(outputDirectory, epochTime + "_nether.zip"));
        	else if (dim == 2)
        		dest = new FileOutputStream(new File(outputDirectory, epochTime + "_the_end.zip"));
        	else
        		return zipDirectory(directoryToCompress, outputDirectory);
        	
            ZipOutputStream zipOutputStream = new ZipOutputStream(dest);

            zipDirectoryHelper(directoryToCompress, directoryToCompress, zipOutputStream);
            zipOutputStream.close();
        } catch (Exception e) {
        	System.out.println("Zip error");
        	Thread.sleep(2000);
        }
        return epochTime;
    }
    
    private static void zipDirectoryHelper (File rootDirectory, File currentDirectory, ZipOutputStream out) throws Exception {
        byte[] data = new byte[2048];

        File[] files = currentDirectory.listFiles();
        if (files == null) {
          // no files were found or this is not a directory

        } else {
            for (File file : files) {
                if (file.isDirectory() && !file.getName().equals("")) {
                    zipDirectoryHelper(rootDirectory, file, out);
                } else {
                    FileInputStream fi = new FileInputStream(file);
                    // creating structure and avoiding duplicate file names
                    String name = file.getAbsolutePath().replace(rootDirectory.getAbsolutePath(), worldFolderName);

                    ZipEntry entry = new ZipEntry(name);
                    out.putNextEntry(entry);
                    int count;
                    BufferedInputStream origin = new BufferedInputStream(fi,2048);
                    while ((count = origin.read(data, 0 , 2048)) != -1){
                        out.write(data, 0, count);
                    }
                    origin.close();
                }
            }
        }
    }

	public static String zipFolder (String src, String dest, String topFolderName) throws InterruptedException {
		return zipDirectory(new File(src), new File(dest), topFolderName);
	}
	
	public static String zipDirectory (File directoryToCompress, File outputDirectory, String topFolderName) throws InterruptedException{
    	String epochTime = Long.toString(Instant.now().getEpochSecond());
        try {
            FileOutputStream dest = new FileOutputStream(new File(outputDirectory, epochTime + ".zip"));
            ZipOutputStream zipOutputStream = new ZipOutputStream(dest);

            zipDirectoryHelper(directoryToCompress, directoryToCompress, zipOutputStream, topFolderName);
            zipOutputStream.close();
        } catch (Exception e) {
        	System.out.println("Zip error");
        	Thread.sleep(2000);
        }
        return epochTime;
    }
	
    private static void zipDirectoryHelper (File rootDirectory, File currentDirectory, ZipOutputStream out, String topFolderName) throws Exception {
        byte[] data = new byte[2048];

        File[] files = currentDirectory.listFiles();
        if (files == null) {
          // no files were found or this is not a directory

        } else {
            for (File file : files) {
                if (file.isDirectory() && !file.getName().equals("")) {
                    zipDirectoryHelper(rootDirectory, file, out, topFolderName);
                } else {
                    FileInputStream fi = new FileInputStream(file);
                    // creating structure and avoiding duplicate file names
                    String name = file.getAbsolutePath().replace(rootDirectory.getAbsolutePath(), topFolderName);

                    ZipEntry entry = new ZipEntry(name);
                    out.putNextEntry(entry);
                    int count;
                    BufferedInputStream origin = new BufferedInputStream(fi,2048);
                    while ((count = origin.read(data, 0 , 2048)) != -1){
                        out.write(data, 0, count);
                    }
                    origin.close();
                }
            }
        }

    }
	
	// Used to copy a directory (replacing existing files)
	public static void copyDirectory (String src, String dest) throws IOException {
		File source = new File (src);
		File destination = new File (dest);
	    if (source.isDirectory()) {
	        if (!destination.exists()) {
	            destination.mkdirs();
	        }
	        String files[] = source.list();
	        for (String file : files) {
	            File srcFile = new File(source, file);
	            File destFile = new File(destination, file);
	            copyDirectory(srcFile.toString(), destFile.toString());
	        }
	    } else {
	        InputStream in = null;
	        OutputStream out = null;
	        try {
	            in = new FileInputStream(source);
	            out = new FileOutputStream(destination);
	            byte[] buffer = new byte[1024];
	            int length;
	            while ((length = in.read(buffer)) > 0)
	            {
	                out.write(buffer, 0, length);
	            }
	        } catch (Exception e) {
	            // Just ignore the error, it's to be expected
	        }
	        in.close();
	        out.close();
	    }
	}
	
	// Used to delete a folder with files
	public static void delDirectory (String target) throws IOException {
		Path directory = Paths.get(target);
		Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
		   @Override
		   public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			   Files.delete(file);
		       return FileVisitResult.CONTINUE;
		   }
		   @Override
		   public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		       Files.delete(dir);
		       return FileVisitResult.CONTINUE;
		   }
		});
	}
	
	// Copies a file from src to dest
	public static void copyFile (String src, String dest) throws IOException {
		Files.copy(Paths.get(src), Paths.get(dest), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
	}
	
	// Splits the input string on the character
	public static ArrayList<String> splitOnChar (String s, String splitChar) {
		return new ArrayList<String> (Arrays.asList(s.split(splitChar)));
	}
	
	// Converts the string to alphanumeric text
	public static String toAlphanumeric (String s) {
		return s.replaceAll("[^A-Za-z0-9]", "");
	}
	
	// Return user's next inputted line as a string
	public static String input () throws InterruptedException, IOException {
		return System.console().readLine();
	}

	// Return user's next inputted line as an int
	public static int inputAsInt () {
		try {
			return Integer.parseInt(System.console().readLine());
		} catch (Exception e) {
			System.out.print("Input invalid.\n  > ");
			return inputAsInt();
		}
	}

	// Return user's next inputted line as an int
	public static int inputAsInt (int min, int max) {
		try {
			int i = Integer.parseInt(System.console().readLine());
			if (i >= min && i <= max)
				return i;
			else {
				System.out.print("Input outside of valid range.\n  > ");
				return inputAsInt(min, max);
			}
		} catch (Exception e) {
			System.out.print("Input invalid.\n  > ");
			return inputAsInt(min, max);
		}
	}
	
	// Runs a process inside of the current console
	public static Process runProcess (String cmd, String dir) throws IOException {
		String[] cmdList = cmd.split(" ");
		ProcessBuilder pb = new ProcessBuilder (cmdList);
		pb.directory(new File(dir));
		return pb.inheritIO().start();
	}
	
	// Returns all files in a folder
	public static ArrayList<String> listFiles (String folder) {
		File f = new File(folder);
		return new ArrayList<String>(Arrays.asList(f.list()));
	}
}
