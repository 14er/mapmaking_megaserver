package com.fourteener.mapmakingserver.executable;

import com.fourteener.mapmakingserver.executable.Util;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class WorldFolderUpdater {
	// Run the world folder updater
    public static void run() throws InterruptedException, IOException {
    	// Get the old profile data
        File profileData = new File("profile_data.mms");
        String rawProfileData = Util.readFromFile(profileData.toString());
        ArrayList<String> profileData2 = Util.splitOnChar(rawProfileData, ";");
        ArrayList<String> profileDataRead = new ArrayList<String>();
        for (String s : profileData2) {
            if (Util.splitOnChar(s, ",").size() != 3) continue;
            profileDataRead.add(s);
        }
        // Change it to the new format
        for (String s : profileDataRead) {
            String profileName = Util.splitOnChar(s, ",").get(0).replaceAll("null", "");
            Util.copyDirectory(profileName + "/server/world", profileName + "/server/" + profileName);
            Util.copyDirectory(profileName + "/server/world_nether", profileName + "/server/" + profileName + "_nether");
            Util.copyDirectory(profileName + "/server/world_the_end", profileName + "/server/" + profileName + "_the_end");
            Util.delDirectory(profileName + "/server/world");
            Util.delDirectory(profileName + "/server/world_nether");
            Util.delDirectory(profileName + "/server/world_the_end");
        }
        Util.writeToFile("wfupdated", false, "");
    }
}