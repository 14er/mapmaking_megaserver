package com.fourteener.mapmakingserver.executable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.jnbt.ByteTag;
import org.jnbt.CompoundTag;
import org.jnbt.NBTInputStream;
import org.jnbt.NBTOutputStream;
import org.jnbt.StringTag;
import org.jnbt.Tag;

public class ProfileEditLaunch {
	
	public static boolean linuxFix = false;
	
	public void profileEditLaunch (String profileFolder, String mcVer) throws Exception {
		while (true) {
			Util.clearConsole();
			
			// Display all options to the user
			System.out.println("Select an option:");
			System.out.println("    1) Launch server from profile");
			System.out.println("    2) Edit profile/server settings");
			System.out.println("    3) Restore snapshot");
			System.out.println("    4) Take world snapshot (restorable backup)");
			System.out.println("    5) Delete all temporary backups and snapshots");
			System.out.println("    6) Extract world save to .zip (publishable)");
			System.out.println("    7) Delete world save");
			System.out.println("    8) Return");
			System.out.print("  > ");
			
			// Get the user's input as an int
			int input = Util.inputAsInt(1, 9);
			
			// Do stuff with the user's input
			if (input == 1) {
				// Launch this profile's server
				launchServer(profileFolder, mcVer);
			} else if (input == 2) {
				// Edit profile settings
				editProfileSettings(profileFolder);
			} else if (input == 3) {
				// Restore world folder from a snapshot
				restoreSnapshot(profileFolder);
			} else if (input == 4) {
				// Create a world snapshot
				takeSnapshot(profileFolder);
			} else if (input == 5) {
				// Delete the backup and snapshot folders
				cleanBackupsSnapshots(profileFolder);
			} else if (input == 6) {
				// Extract the world save (cleaning up & changing NBT as to be publishable)
				extractWorldSave(profileFolder);
			} else if (input == 7) {
				// Extract the world save (cleaning up & changing NBT as to be publishable)
				deleteWorldSave(profileFolder);
			} else if (input == 8) {
				// Head back to the profile manager
				return;
			}
		}
	}
	
	private void launchServer (String profileFolder, String mcVer) throws Exception {
		// First, we update the server.properties file; then we run the server :D
		updateServerDotProperties(profileFolder);
		String ramAmt = Util.splitOnChar(Util.readFromFile(profileFolder + "/profileProperties.mms"), "\n").get(0);
		Util.clearConsole();
		// If using 1.13.2; do some more stuff before launching the server.
		Process p = null;
		
		String args = "-DIReallyKnowWhatIAmDoingISwear";
		if (linuxFix) {
			args = args + " --nojline";
		}
		
		if (mcVer.equals("1.13.2")) {
			System.out.print("Select a version to launch:\n    1) FAWE/VS Functional"
					+ "\n    2) NBTEditor Functional"
					+ "\n    3) Return"
					+ "\n  > ");
			
			int input = Util.inputAsInt(1,3);
			Util.clearConsole();
			if (input == 1) {
				p = Util.runProcess ("java -jar -Xmx" + ramAmt + "M -Xms" + ramAmt + "M " + args + " server-paper.jar", profileFolder + "/server");
			}
			else if (input == 2) {
				p = Util.runProcess ("java -jar -Xmx" + ramAmt + "M -Xms" + ramAmt + "M " + args + " server-spigot.jar", profileFolder + "/server");
			}
			else return;
		}
		else {
			p = Util.runProcess ("java -jar -Xmx" + ramAmt + "M -Xms" + ramAmt + "M " + args + " server.jar", profileFolder + "/server");
		}
		int exitCode = p.waitFor();
		if (exitCode != 0) {
			System.out.println("\n\n\nThe server appears to have terminated abnormally.\nWe will return to the profile page in 15 seconds.");
			Thread.sleep(15000);
		}
		backupServer(profileFolder);
		try {
			Util.delDirectory(profileFolder + "/server/logs");
		} catch (Exception e) {
			// Really, just ignore this error
		}
	}
	
	private void editProfileSettings (String profileFolder) throws IOException, InterruptedException {
		Util.clearConsole();
		System.out.println("Editing profile/server options. Press enter to keep old setting.\nWARNING: These values are not checked for validity.");
		ArrayList<String> options = Util.splitOnChar(Util.readFromFile(profileFolder + "/profileProperties.mms"), "\\R+");
		System.out.print("RAM to allocate (current: " + options.get(0) + "): ");
		String in = Util.input().replaceAll("null", "");
		if (!(in == null || in.equals("")))
			options.set(0, in.replaceAll("null", ""));
		System.out.print("Server MOTD (current: " + options.get(1) + "): ");
		in = Util.input();
		if (!(in == null || in.equals("")))
			options.set(1, in.replaceAll("null", ""));
		System.out.print("Superflat preset (current: " + options.get(2) + "): ");
		in = Util.input();
		if (!(in == null || in.equals("")))
			options.set(2, in.replaceAll("null", ""));
		System.out.print("Difficulty [peaceful, easy, normal, hard] (current: " + options.get(3) + "): ");
		in = Util.input();
		if (!(in == null || in.equals("")))
			options.set(3, in.replaceAll("null", ""));
		System.out.print("Max server render distance (current: " + options.get(4) + "): ");
		in = Util.input();
		if (!(in == null || in.equals("")))
			options.set(4, in.replaceAll("null", ""));
		System.out.print("Max players on server (current: " + options.get(5) + "): ");
		in = Util.input();
		if (!(in == null || in.equals("")))
			options.set(5, in.replaceAll("null", ""));
		try {
			System.out.print("Server port (current: " + options.get(6) + "): ");
			in = Util.input();
			if (!(in == null || in.equals("")))
				options.set(6, in.replaceAll("null", ""));
		} catch (Exception e) {
			System.out.print("Server port (current: 25565): ");
			in = Util.input();
			if (!(in == null || in.equals("")))
				options.add(in.replaceAll("null", ""));		
			else
				options.add(6, "25565");	
		}
		Util.writeToFile(profileFolder + "/profileProperties.mms", false, options, "\n");
	}
	
	private void restoreSnapshot (String profileFolder) throws InterruptedException, IOException {
		boolean foundSnapshots = true;
		ArrayList<String> snapshots = new ArrayList<String>();
		ArrayList<String> snapshotZips = new ArrayList<String>();
		try {
			snapshots = Util.listFiles(profileFolder + "/snapshots");
			for (String s : snapshots) {
				if (s.contains("_nether")) {
					snapshotZips.add(s.replace((CharSequence) "_nether", ""));
				}
			}
		} catch (Exception e) {
			foundSnapshots = false;
		}
		System.out.println("Please select a UNIX timestamp to restore a snapshot from.\nWARNING: Restoring a snapshot will irreversably delete your current world file");
		int i = 1;
		if (foundSnapshots) {
			for (String s : snapshotZips) {
				s = s.replaceAll(".zip", "");
				System.out.println("    " + Integer.toString(i++) + ") " + s);
			}
		}
		System.out.println("    " + Integer.toString(i) + ") Return without restoring a snapshot");
		System.out.print("  > ");
		int in = Util.inputAsInt();
		if (in >= i)
			return;
		else {
			if ((new File(profileFolder + "/server/" + Util.worldFolderName).exists())) {
				Util.delDirectory(profileFolder + "/server/" + Util.worldFolderName);
				Util.delDirectory(profileFolder + "/server/" + Util.worldFolderName + "_nether");
				Util.delDirectory(profileFolder + "/server/" + Util.worldFolderName + "_the_end");
			}
			Util.unzipFile(profileFolder + "/snapshots/" + snapshotZips.get(in-1).replaceAll(".zip", "") + "_nether.zip", profileFolder + "/server");
			Util.copyDirectory(profileFolder + "/server/" + Util.worldFolderName, profileFolder + "/server/" + Util.worldFolderName + "_nether");
			Util.delDirectory(profileFolder + "/server/" + Util.worldFolderName);
			Util.unzipFile(profileFolder + "/snapshots/" + snapshotZips.get(in-1).replaceAll(".zip", "") + "_the_end.zip", profileFolder + "/server");
			Util.copyDirectory(profileFolder + "/server/" + Util.worldFolderName, profileFolder + "/server/" + Util.worldFolderName + "_the_end");
			Util.delDirectory(profileFolder + "/server/" + Util.worldFolderName);
			Util.unzipFile(profileFolder + "/snapshots/" + snapshotZips.get(in-1), profileFolder + "/server");
		}
	}
	
	private void takeSnapshot (String profileFolder) throws Exception {
		File levelDat = new File(profileFolder + "/server/" + Util.worldFolderName + "/level.dat");
		if (!levelDat.exists()) {
			System.out.println("No world data found.");
			Thread.sleep(2500);
			return;
		}
		File f = new File(profileFolder + "/snapshots");
		f.mkdirs();
		String epoch = Util.zipFolder(profileFolder + "/server/" + Util.worldFolderName, profileFolder + "/snapshots", 0);
		String epoch1 = Util.zipFolder(profileFolder + "/server/" + Util.worldFolderName + "_nether", profileFolder + "/snapshots", 1);
		String epoch2 = Util.zipFolder(profileFolder + "/server/" + Util.worldFolderName + "_the_end", profileFolder + "/snapshots", 2);
		Util.moveFile(profileFolder + "/snapshots/" + epoch1 + "_nether.zip", profileFolder + "/snapshots/" + epoch + "_nether.zip");
		Util.moveFile(profileFolder + "/snapshots/" + epoch2 + "_the_end.zip", profileFolder + "/snapshots/" + epoch + "_the_end.zip");
		System.out.println("Snapshot taken. UNIX timestamp: " + epoch);
		Thread.sleep(5000);
	}
	
	private void cleanBackupsSnapshots (String profileFolder) throws InterruptedException, IOException {
		System.out.print("Type 'yes' without quotes to delete all backups and snapshots.\n  > ");
		String in = Util.input();
		if (in.toLowerCase().equals("yes")) {
			if ((new File(profileFolder + "/backups")).exists())
				Util.delDirectory(profileFolder + "/backups");
			if ((new File(profileFolder + "/snapshots")).exists())
				Util.delDirectory(profileFolder + "/snapshots");
			System.out.println("Snapshots and backups deleted.");
			Thread.sleep(2500);
		}
	}
	
	private void extractWorldSave (String profileFolder) throws Exception {
		try {
			// Check that there is a world save to export
			File levelDat = new File(profileFolder + "/server/" + Util.worldFolderName + "/level.dat");
			if (!levelDat.exists()) {
				System.out.println("No world data found.");
				Thread.sleep(2500);
				return;
			}
			
			// First, we have to determine the server's root path.
			ArrayList<String> pathParts = Util.splitOnChar((new File(profileFolder)).getAbsolutePath(), "[/\\\\]");
			boolean adding = true;
			String serverRootPath = "";
			for (String s : pathParts) {
				if (!s.equals("server_data") && adding)
					serverRootPath += s + "/";
				else if (s.equals("server_data"))
					adding = false;
			}
			
			// Let's copy the world folder (and Nether/End) to the root folder temporarily (so we can clean it up and edit NBT data)
			Util.copyDirectory(profileFolder + "/server/" + Util.worldFolderName, serverRootPath + "/" + Util.worldFolderName);
			Util.copyDirectory(profileFolder + "/server/" + Util.worldFolderName + "_nether/DIM-1", serverRootPath + "/" + Util.worldFolderName + "/DIM-1");
			Util.copyDirectory(profileFolder + "/server/" + Util.worldFolderName + "_the_end/DIM1", serverRootPath + "/" + Util.worldFolderName + "/DIM1");
			
			// Clean up unneeded stuff from the world folder
			try {
				Util.delDirectory(serverRootPath + "/" + Util.worldFolderName + "/advancements");
			} catch (Exception e) {}
			try {
				Util.delDirectory(serverRootPath + "/" + Util.worldFolderName + "/playerdata");
			} catch (Exception e) {}
			try {
				Util.delDirectory(serverRootPath + "/" + Util.worldFolderName + "/stats");
			} catch (Exception e) {}
			try {
				Files.delete(Paths.get(serverRootPath + "/" + Util.worldFolderName + "/level.dat_old"));
			} catch (Exception e) {}
			try {
				Files.delete(Paths.get(serverRootPath + "/" + Util.worldFolderName + "/session.lock"));
			} catch (Exception e) {}
			
			// Let's get some inputs from the user!
			System.out.println("Some information is required for exporting the world data.\nWARNING: This data is not checked for validity. Please obey the comments about valid values.\n"
					+ "Data to verify in-game: Starting location, starting spawnpoint, starting game mode");
			System.out.print("Zip file name [safe file name, no extension]: ");
			String zipFileName = Util.input();
			System.out.print("World folder name [safe file name]: ");
			String worldFolderName = Util.input();
			System.out.print("Level name (in-game) [safe level name]: ");
			String levelName = Util.input();
			System.out.print("Allow commands? [0 or 1]: ");
			String allowCommands = Util.input();
			System.out.print("Level difficulty [0, 1, 2, or 3]: ");
			String levelDifficulty = Util.input();
			System.out.print("Difficulty locked? [0 or 1]: ");
			String difficultyLocked = Util.input();
			System.out.print("Hardcore? [0 or 1]: ");
			String hardcore = Util.input();
			
			// Edit level.dat to have the needed NBT
			// First, create the needed tags with user-input values
			StringTag levName = new StringTag("LevelName", levelName);
			ByteTag allCommands = new ByteTag("allowCommands", Byte.parseByte(allowCommands));
			ByteTag lvlDifficulty = new ByteTag("Difficulty", Byte.parseByte(levelDifficulty));
			ByteTag diffLocked = new ByteTag("DifficultyLocked", Byte.parseByte(difficultyLocked));
			ByteTag hcore = new ByteTag("hardcore", Byte.parseByte(hardcore));
			
			// Next, read the compound NBT tag from level.dat
			InputStream is = new FileInputStream(new File(serverRootPath + "/" + Util.worldFolderName + "/level.dat"));
			NBTInputStream nbtis = new NBTInputStream(is);
			CompoundTag originalTopLevelTag = (CompoundTag) nbtis.readTag();
			Map<String, Tag> originalData =
					((CompoundTag) originalTopLevelTag.getValue().get("Data")).getValue();
			Map<String, Tag> newData = new LinkedHashMap<String, Tag>(originalData);
			nbtis.close();
			is.close();
			
			// Edit the data tag
			newData.put("LevelName", levName);
			newData.put("allowCommands", allCommands);
			newData.put("Difficulty", lvlDifficulty);
			newData.put("DifficultyLocked", diffLocked);
			newData.put("hardcore", hcore);
			newData.remove("Player");
			CompoundTag outData = new CompoundTag("Data", newData);
			Map<String, Tag> outDataLoneMap = new LinkedHashMap<String, Tag>();
			outDataLoneMap.put("Data", outData);
			CompoundTag outData2 = new CompoundTag("Data", outDataLoneMap);
			
			// Output the updated NBT
			OutputStream os = new FileOutputStream(new File(serverRootPath + "/" + Util.worldFolderName + "/level.dat"));
			NBTOutputStream nbtos = new NBTOutputStream(os);
			nbtos.writeTag(outData2);
			nbtos.close();
			os.close();
			
			// Lets rename the world folder now
			Util.copyDirectory(serverRootPath + "/" + Util.worldFolderName, serverRootPath + "/" + worldFolderName);
			// Then zip it up
			String epochTime = Util.zipFolder(serverRootPath + "/" + worldFolderName, serverRootPath, worldFolderName);
			if ((new File(serverRootPath + "/" + zipFileName + ".zip")).exists())
				Util.delDirectory(serverRootPath + "/" + zipFileName + ".zip");
			Files.move(Paths.get(serverRootPath + "/" + epochTime + ".zip"),
					Paths.get(serverRootPath + "/" + epochTime + ".zip").resolveSibling(serverRootPath + "/" + zipFileName + ".zip"));
			
			// Time to clean up everything :D
			Util.delDirectory(serverRootPath + "/" + Util.worldFolderName);
			try {
				Util.delDirectory(serverRootPath + "/" + worldFolderName);
			} catch (Exception e) {
				// And ignore it
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("\n\n\nWorld Export Failed.\nAre the input values valid?\nIs there an existing exported world zip with that name?");
			System.out.println("Will continue in 15 seconds.");
			Thread.sleep(15000);
			Util.clearConsole();
		}
	}
	
	private void deleteWorldSave (String profileFolder) throws IOException, InterruptedException {
		File levelDat = new File(profileFolder + "/server/" + Util.worldFolderName + "/level.dat");
		if (!levelDat.exists()) {
			System.out.println("No world data found.");
			Thread.sleep(2500);
			return;
		}
		System.out.println("Delete the world save? This CANNOT be undone.\nType \"Y\" without quotes to confirm.");
		String in = Util.input();
		if (in.equals("Y")) {
			System.out.println("Are you really, positively sure?\nType \"Y\" without quotes again to confirm.");
			in = Util.input();
			if (in.equals("Y")) {
				Util.delDirectory(profileFolder + "/server/" + Util.worldFolderName);
				Util.delDirectory(profileFolder + "/server/" + Util.worldFolderName + "_nether");
				Util.delDirectory(profileFolder + "/server/" + Util.worldFolderName + "_the_end");
			}
		}
	}
	
	private void updateServerDotProperties (String profileFolder) throws IOException {
		ArrayList<String> options = Util.splitOnChar(Util.readFromFile(profileFolder + "/profileProperties.mms"), "\\R+");
		String serverProps = GlobalVars.remainingServerProperties;
		serverProps += "motd=" + options.get(1) + "\n";
		serverProps += "generator-settings=" + options.get(2) + "\n";
		serverProps += "difficulty=" + options.get(3) + "\n";
		serverProps += "view-distance=" + options.get(4) + "\n";
		serverProps += "max-players=" + options.get(5) + "\n";
		serverProps += "online-mode=" + Util.internetConnected() + "\n";
		serverProps += "level-name=" + Util.worldFolderName + "\n";
		try {
			serverProps += "server-port=" + options.get(6);
		} catch (Exception e) {
			serverProps += "server-port=25565";
		}
		Util.writeToFile(profileFolder + "/server/server.properties", false, serverProps);
	}
	
	private void backupServer (String profileFolder) throws Exception {
		File f = new File(profileFolder + "/backups");
		f.mkdirs();
		Util.zipFolder(profileFolder + "/server/" + Util.worldFolderName, profileFolder + "/backups", 0);
		Util.zipFolder(profileFolder + "/server/" + Util.worldFolderName + "_nether", profileFolder + "/backups", 1);
		Util.zipFolder(profileFolder + "/server/" + Util.worldFolderName + "_the_end", profileFolder + "/backups", 2);
	}
}
