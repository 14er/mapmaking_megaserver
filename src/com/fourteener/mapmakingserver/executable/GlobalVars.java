package com.fourteener.mapmakingserver.executable;

public class GlobalVars {
	public static String defaultProfileSettings = "2048\n" // RAM in MB
			+ "Mapmaking Megaserver from 14er\n" // MOTD
			+ "3;7,239*24;1;\n" // Superflat preset
			+ "peaceful\n" // Difficulty
			+ "10\n" // Render distance
			+ "20\n" // Max online players
			+ "25565"; // Server port
	public static String remainingServerProperties = 
			"spawn-protection=0\n" +
			"force-gamemode=false\n" +
			"allow-nether=true\n" +
			"gamemode=survival\n" +
			"broadcast-console-to-ops=true\n" +
			"enable-query=false\n" +
			"player-idle-timeout=0\n" +
			"spawn-monsters=true\n" +
			"op-permission-level=4\n" +
			"announce-player-achievements=true\n" +
			"pvp=true\n" +
			"snooper-enabled=false\n" +
			"level-type=FLAT\n" +
			"hardcore=false\n" +
			"enable-command-block=true\n" +
			"network-compression-threshold=256\n" +
			"resource-pack-sha1=\n" +
			"max-world-size=29999984\n" +
			"debug=false\n" +
			"server-ip=\n" +
			"spawn-npcs=true\n" +
			"allow-flight=false\n" +
			"spawn-animals=true\n" +
			"white-list=true\n" +
			"generate-structures=false\n" +
			"max-build-height=256\n" +
			"level-seed=\n" +
			"prevent-proxy-connections=false\n" +
			"enable-rcon=false\n" +
			"resource-pack=\n" +
			"broadcast-rcon-to-ops=true";
}
