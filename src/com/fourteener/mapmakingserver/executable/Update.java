package com.fourteener.mapmakingserver.executable;

import java.util.ArrayList;

public class Update {
	// All server version downloads should be formatted as follows:
	// URL,versionNumber,sizeInMB
	
	public void manageUpdates (boolean forceUpdate) throws Exception {
		Util.clearConsole();
		while (true) {
			// First off, verify that the user is connected to the internet, and return with a warning if user is not
			if (!Util.internetConnected() && !forceUpdate) {
				System.out.println("This operation requires internet. Please connect to the internet to continue.");
				Thread.sleep(3000);
				return;
			}
			if (!Util.internetConnected() && forceUpdate) {
				System.out.println("This operation requires internet. Please connect to the internet to continue.");
				Thread.sleep(5000);
				continue;
			}
			
			// From here on out, we can assume the user still has internet
			// First, we'll grab the update data
			ArrayList<String> gottenUpdates = getUpdates();
			
			// Then, we'll list off the available versions and get user input (resolving it to a version)
			while (true) {
				System.out.println("Please select a version to install.\nSelecting an already installed version will update it.\nTo select a version, type the version name as it appears.\nYou may only install one of the below-listed versions (or sets) at a time.");
				System.out.println("    Versions available: ");
				for (String s : gottenUpdates) {
					if (parseUpdate(s).size() == 3)
						System.out.println("    " + parseUpdate(s).get(1) + ", Size: " + parseUpdate(s).get(2) + "MB");
				}
				if (!forceUpdate)
					System.out.println("Or, type 'return' to exit the download manager.");
				System.out.print("  > ");
				String userInput = Util.input();
				if (userInput.toLowerCase().equals("return") && !forceUpdate) {
					Util.clearConsole();
					return;
				}
				for (String s : gottenUpdates) {
					// Once we have a valid version selected, download and store it
					if (parseUpdate(s).size() == 3)
					if (userInput.equals(parseUpdate(s).get(1))) {
						System.out.println("\n\n\nDownloading...\nThis may take a while. Please be patient.");
						Util.downloadUnzipCopy(parseUpdate(s).get(0), "downloaded_versions");
						return;
					}
				}
				System.out.println("Invalid input.");
				Thread.sleep(2500);
				Util.clearConsole();
			}
		}
	}
	
	// Gets the text of the updates txt
	private ArrayList<String> getUpdates () throws Exception {
		String s = Util.getTextFromURL (Util.downloadURL);
		return Util.splitOnChar(s, ";");
	}
	
	// Splits the update String s into useful components
	private ArrayList<String> parseUpdate (String s) {
		return Util.splitOnChar(s, ",");
	}
}
