package com.fourteener.mapmakingserver.executable;

import com.fourteener.mapmakingserver.executable.ProfileManager;
import com.fourteener.mapmakingserver.executable.Update;
import com.fourteener.mapmakingserver.executable.Util;
import com.fourteener.mapmakingserver.executable.WorldFolderUpdater;
import java.io.File;
import java.io.IOException;

public class Main {
    static boolean betaBuild = false;

    // The main function is a main function
    public static void main(String[] args) throws InterruptedException {
    	// Gets if the launcher is updated, and if it supports beta mode
        boolean hasUpdatedLauncher = false;
        boolean betaLauncher = false;
        // Various checks for updated launcher, offline mode, beta mode, etc.
        for (String s : args) {
            if (s.equals("updated3")) {
                hasUpdatedLauncher = true;
            }
            if (s.equals("-ForceOffline")) {
                Util.forceOffline = true;
            }
            if (!s.equals("betaMode"))
            	betaLauncher = true;
            if (s.equals("-LinuxFix")) {
            	ProfileEditLaunch.linuxFix = true;
            }
        }
        // Informs the user of any irregularities
        if (!hasUpdatedLauncher) {
            System.out.println("Please redownload the server launcher, for technical updates.");
            System.out.println("The server will launch in 30 seconds.");
            Thread.sleep(30000L);
        }
        if (!betaLauncher && betaBuild) {
            System.out.println("You are using a beta build but do not have the most recent launcher.\nPlease redownload the launcher from http://14erc.com/mms");
            System.out.println("The server will launch in 30 seconds.");
            Thread.sleep(30000L);
        }
        if (!betaLauncher && new File("betaMode").exists()) {
            System.out.println("You have opted into beta builds, but have an outdated launcher.\nPlease redownload the launcher from http://14erc.com/mms");
            System.out.println("The server will launch in 30 seconds.");
            Thread.sleep(30000L);
        }
      
        // Splash-screen text
        System.out.println("The Mapmaking Megaserver from 14er");
        System.out.println("Universal Java Edition\n");
        System.out.println("http://14erc.com/mms");
        System.out.println("Licensing, terms, disclaimers, etc. on website");
        System.out.println("Launcher, executable, and website (C)2019 Logan Cooper");
        if (betaBuild) {
            System.out.println("\nBeta build. Please exercise caution.");
        }
        Thread.sleep(5000L);
        Util.clearConsole();
      
        // Attempt to update the world folder layout if needed
        do {
            File f;
            try {
                f = new File("wfupdated");
                File f2 = new File("profile_data.mms");

                // If an update is needed, perform the update
                if (!f.exists() && f2.exists()) {
                    System.out.println("This update requires a change to the format of the server's internal data.");
                    System.out.println("Nothing should happen, but should you desire to make a backup, do so now.");
                    Thread.sleep(10000L);
                    System.out.println("Press enter to continue.");
                    Util.input();
                    WorldFolderUpdater.run();
                }
                Util.clearConsole();
            }
            // Something went wrong with the update, the server needs to terminate
            catch (Exception e) {
                System.out.println("Uh oh! That did not work! Hopefully no data was lost. Please screenshot the below message and send it to 14er.\n\n");
                e.printStackTrace();
                try {
					Util.writeToFile("error.log", true, e.getMessage() + "\n\n");
				} catch (IOException e1) {
					System.out.println("\n\n");
					e1.printStackTrace(); // If this is triggered, that would be hilarious
				}
                System.out.println("\n\n\nServer will close in 30 seconds.");
                Thread.sleep(30000L);
                return;
            }
            
            // Handles launching the server and other user-end stuff
            try {
            	  // Make sure they have a usable server version to run
                f = new File("downloaded_versions");
                if (!f.exists()) {
                    System.out.println("Cannot find a downloaded server version. Sending you to the download server versions page.");
                    Thread.sleep(2500L);
                    Update updater = new Update();
                    updater.manageUpdates(true);
                    updater = null;
                    Util.clearConsole();
                }

                // User interface
                System.out.println("Please select an option by typing the corresponding number.");
                System.out.println("    1) Run server");
                System.out.println("    2) Download server versions");
                if (!new File("betaMode").exists()) {
                    System.out.println("    3) Enable beta mode");
                } else {
                    System.out.println("    3) Disable beta mode");
                }
                System.out.println("    4) Exit the server");
                System.out.print("  > ");
                // Grab and parse input
                int input = Util.inputAsInt(1, 4);
                // Start running the server from profile manager
                if (input == 1) {
                    ProfileManager pf = new ProfileManager();
                    pf.doProfileManagement();
                    pf = null;
                    // Download more Minecraft server versions
                } else if (input == 2) {
                    Update updater = new Update();
                    updater.manageUpdates(false);
                    updater = null;
                // This handles beta mode
                } else if (input == 3) {
                    String in;
                    boolean inBetaMode = new File("betaMode").exists();
                    // Disables beta mode
                    if (inBetaMode) {
                        System.out.print("This will disable beta mode and send you back to the most recent stable build.\nAny beta features will no longer be accessable, and you may lose access to some data.\nType 'yes' without quotes to disable beta mode.\n    > ");
                        in = Util.input();
                        if (in.equals("yes")) {
                            Util.delDirectory("betaMode");
                            Util.writeToFile("exec_ver.mms", false, "1");
                            System.out.println("\nBeta mode has been disabled. Please restart the server to return to a stable version.");
                            Thread.sleep(10000L);
                        }
                    // Enables beta mode
                    } else {
                        System.out.print("Beta mode allows you to test new features before they are released.\nThese build may be highly unstable, and may cause loss of data.\nPlease excersise caution. 14ercooper is not responsible for any damages caused by this server.\n\n");
                        System.out.print("Type 'yes' without quotes to enable beta mode.\n    > ");
                        in = Util.input();
                        if (in.equals("yes")) {
                            Util.writeToFile("betaMode", false, "");
                            System.out.println("\nBeta mode has been enabled. Please restart the server to download the beta executable.");
                            Thread.sleep(10000L);
                        }
                    }
                // Exits the server
                } else if (input == 4) break;
                Util.clearConsole();
            }

            // Catches crashes and restarts
            catch (Exception e) {
                System.out.println("Uh oh! Something went horribly wrong!\nPlease screenshot the below error and send it to 14er!\n\n");
                e.printStackTrace();
                try {
					Util.writeToFile("error.log", true, e.getMessage() + "\n\n");
				} catch (IOException e1) {
					System.out.println("\n\n");
					e1.printStackTrace(); // If this is triggered, that would be hilarious
				}
                System.out.println("\n\n\nServer will relaunch in 30 seconds.");
                Thread.sleep(30000L);
                Util.clearConsole();
            }
        } while (true);
    }
}