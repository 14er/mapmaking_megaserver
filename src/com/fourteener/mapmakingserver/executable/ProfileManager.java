package com.fourteener.mapmakingserver.executable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class ProfileManager {
	public void doProfileManagement () throws Exception {		
		// A loop for doing profile management stuff (open profile, create profile, delete profile, return to main loop)
		while (true) {// Loads the profile data to an array of strings. Each string has the following format: name,version,folder
			// Makes sure at least one profile exists at all times
			File profileData = new File("profile_data.mms");
			if (!profileData.exists()) {
				Util.writeToFile(profileData.toString(), false, "");
				Util.clearConsole();
				System.out.println("No profile data detected. Please create a new profile.");
				createProfile(new ArrayList<String>());
			}
			
			Util.clearConsole();
			String rawProfileData = Util.readFromFile(profileData.toString());
			ArrayList<String> profileData2 = Util.splitOnChar(rawProfileData, ";");
			ArrayList<String> profileDataRead = new ArrayList<String>();
			for (String s : profileData2) {
				if (Util.splitOnChar(s, ",").size() == 3)
					profileDataRead.add(s);
			}
			
			System.out.println("Choose an option.");
			
			int i = 1;
			for (String s : profileDataRead) {
				String str = Util.splitOnChar(s, ",").get(0).replaceAll("null", "");
				String ver = Util.splitOnChar(s, ",").get(1);
				System.out.println("    " + Integer.toString(i++) + ") Open profile: " + str + " (Version: " + ver + ")");
			}
			System.out.println("    " + Integer.toString(i) + ") Create new profile");
			System.out.println("    " + Integer.toString(++i) + ") Delete profile");
			System.out.println("    " + Integer.toString(++i) + ") Change version installed on profile");
			System.out.println("    " + Integer.toString(++i) + ") Return");
			System.out.print("  > ");
			
			// Gets the user's input as an int
			int input = Util.inputAsInt(1, i);
			
			// Deal with the input accordingly
			if (input == i) // User wants to exit, so return to main loop
				return;
			else if (input == i-2) {
				deleteProfile(profileDataRead);
			}
			else if (input == i-3) {
				createProfile(profileDataRead);
			} else if (input == i-1) {
				changeProfileVersion(profileDataRead);
			} else if (input > 0) {
				openProfile(profileDataRead.get(input-1));
			}

			// Finally, clear the console for the next iteration
			Util.clearConsole();
		}
	}
	
	// Open the selected profile
	private void openProfile (String profileData) throws Exception {
		ProfileEditLaunch pel = new ProfileEditLaunch();
		Util.worldFolderName = Util.splitOnChar(profileData, ",").get(2);
		String mcVersion = Util.splitOnChar(profileData, ",").get(1);
		pel.profileEditLaunch(Util.worldFolderName, mcVersion);
		pel = null;
	}
	
	// Create a new profile & save the data to the profile data file
	private void createProfile (ArrayList<String> profileDataRead) throws IOException, InterruptedException {
		// Marks as updated to world name system
		Util.writeToFile("wfupdated", false, "");
		
		// Gets the name of the profile from the user
		String profileName = null;
		while (true) {
			System.out.print("Please name your new profile. Alphanumeric only.\n  > ");
			profileName = Util.toAlphanumeric(Util.input());
			File f = new File(Util.toAlphanumeric(profileName));
			if (!f.exists())
				break;
			else {
				System.out.println("That profile name cannot be used (name already exists).");
				Thread.sleep(2500);
				Util.clearConsole();
			}
		}
		
		// Gets the version to install into the server from the user
		String version = null;
		File verFolder = new File("downloaded_versions");
		ArrayList<String> verZips = new ArrayList<String>(Arrays.asList(verFolder.list()));
		System.out.println("Please select a version to install to this profile.");
		int i = 1;
		for (String s : verZips) {
			System.out.println("    " + Integer.toString(i++) + ") " + s.replaceAll(".zip", ""));
		}
		System.out.print("  > ");
		
		int input = Util.inputAsInt(1, i-1);
		version = verZips.get(input-1).replaceAll(".zip", "");
	
		// Creates the profile folder & default profile properties file
		String folderName = profileName;
		File f = new File (folderName);
		f.mkdirs();
		Util.unzipFile("downloaded_versions/" + version + ".zip", folderName + "/server");
		Util.writeToFile(folderName + "/profileProperties.mms", false, GlobalVars.defaultProfileSettings);
		
		// Saves the profile data to profile data file
		profileDataRead.add(profileName + "," + version + "," + folderName);
		Util.writeToFile("profile_data.mms", false, profileDataRead, ";");
	}
	
	// Delete an existing profile & save the data to the profile data file
	private void deleteProfile (ArrayList<String> profileDataRead) throws InterruptedException, IOException {
		System.out.println("Delete which profile?");
		int i = 1;
		for (String s : profileDataRead) {
			String str = Util.splitOnChar(s, ",").get(0).replaceAll("null", "");
			System.out.println("    " + Integer.toString(i++) + ") " + str);
		}
		System.out.println("    " + Integer.toString(i) + ") Cancel");
		System.out.print("  > ");
		
		// Get the user's input as an int
		int input = Util.inputAsInt(1, i);
		
		if (input == i) // User regrets this choice, so go back
			return;
		else if (input > 0) { // User selected a profile to delete.
			// Confirm the deletion before deleting it
			System.out.println("THIS ACTION WILL DELETE ALL DATA (INCLUDING WORLD AND BACKUPS) AND CANNOT BE UNDONE!\nAre you really sure about this? Enter 'yes' without quotes to confirm.");
			String userInput = Util.input();
			if (userInput.toLowerCase().equals("yes")) { // User really wants to do this, do delete it
				System.out.println("Deleting profile...\nMay it rest in peace.");
				String data = profileDataRead.get(input-1);
				// Step one, delete the file
				Util.delDirectory(Util.splitOnChar(data, ",").get(2));
				// Step two, delete the data from profile data
				profileDataRead.remove(input-1);
				// Set three, save the updated profile data file
				if (profileDataRead.size() >= 1)
					Util.writeToFile("profile_data.mms", false, profileDataRead, ";");
				else {
					File f = new File("profile_data.mms");
					f.delete();
				}
				Thread.sleep(1000);
			} else // User really regrets their choice, so go back
				return;
		} else if (input == 0) { // User's input is invalid
			System.out.println("Invalid input.");
			Thread.sleep(2500);
			return;
		}
	}
	
	// Updates or changes the version installed in a profile
	private void changeProfileVersion (ArrayList<String> profileDataRead) throws InterruptedException, IOException {
		System.out.println("Update/change which profile's version?");
		int i = 1;
		for (String s : profileDataRead) {
			String str = Util.splitOnChar(s, ",").get(0).replaceAll("null", "");
			System.out.println("    " + Integer.toString(i++) + ") " + str);
		}
		System.out.println("    " + Integer.toString(i) + ") Cancel");
		System.out.print("  > ");
		
		// Get the user's input as an int
		int input = Util.inputAsInt(1, i);
		
		if (input == i) // Return if the user doesn't want to change versions
			return;
		
		// Gets the version to install into the server from the user
		String version = null;
		File verFolder = new File("downloaded_versions");
		ArrayList<String> verZips = new ArrayList<String>(Arrays.asList(verFolder.list()));
		System.out.println("Please select a version to install to this profile. This cannot be changed later.");
		int j = 1;
		for (String s : verZips) {
			System.out.println("    " + Integer.toString(j++) + ") " + s.replaceAll(".zip", ""));
		}
		System.out.print("  > ");
		
		int verInput = Util.inputAsInt(1, j-1);
		version = verZips.get(verInput-1).replaceAll(".zip", "");
		
		// Clean up the old server install
		String profileData = profileDataRead.get(input-1);
		String profileFolder = Util.splitOnChar(profileData, ",").get(2);
		Files.delete(Paths.get(profileFolder + "/server/server.jar"));
		Files.delete(Paths.get(profileFolder + "/server/eula.txt"));
		Util.delDirectory(profileFolder + "/server/plugins");
		
		// Install the new version into the profile
		Util.unzipFile("downloaded_versions/" + version + ".zip", profileFolder + "/server");
		
		// Update the server properties file
		profileDataRead.set(input-1, Util.splitOnChar(profileDataRead.get(input-1),",").get(0) + "," + version + "," +  Util.splitOnChar(profileDataRead.get(input-1),",").get(2));
		Util.writeToFile("profile_data.mms", false, profileDataRead, ";");
		
		// And return w/ a confirmation
		System.out.println("Profile version updated.");
		Thread.sleep(2500);
	}
}
